#include <stdio.h>
#include "src/cl_args.h"
#include "src/color.h"
#include "src/link_hashmap.h"

int main(int argc, char **argv) {
	printf(GRN "Query : %s\n" RESET, get_args(argc, argv));
}
