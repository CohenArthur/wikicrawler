#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#define BUFFER_SIZE 4096

/*
 * Displays help regarding the program's options
 * Utilisation :
 *      print_help();
 */
void print_help();

/*
 * Analyses the args passed, returns the search query or prints the help
 * Utilisation :
 *      char *search_query = get_args(argv, argc, &ma_verbose, &mon_encode);
 *      ...
 *      free(search_query);
 */
char *get_args(int argv, char **argc);

