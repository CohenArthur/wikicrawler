#include "cl_args.h"

 void print_help() {
 	printf("DESCRIPTION : EfT WikiCrawler - Quickly find a specific expression on all the pages it is present\n\n");
	printf("HOW TO USE : ./crawler \"[expression]\" OR ./crawler -h/--help\n\n");
	printf("OPTIONS :\n""\t-h, --help : Displays this help\n");
}

//#include "file_operations.h"

char *get_args(int argv, char **argc) {
	char *search_query = calloc(1, BUFFER_SIZE);

	if (argv != 2) {
		print_help();
		errx(1, "Incorrect number of arguments");
	}
	if ((strcmp(*(argc + 1), "-h") == 0) || (strcmp(*(argc + 1), "--help") == 0)) {
			print_help();
			exit(0);
	} else {
		strcpy(search_query, *(argc + 1));
	}

	return search_query;
}

