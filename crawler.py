#!/bin/python3

from bs4 import BeautifulSoup as bs
import urllib.request as req
import sys
import re
from hashlib import md5

class bColors:
    BLU = '\033[94m'
    GRN = '\033[92m'
    WAR = '\033[93m'
    RED = '\033[91m'
    RST = '\033[0m'


log_file = open("result_log.html", "w+")
log_file.write(
        "<!DOCTYPE html>\n<html>\n<body bgcolor=#1c1a18>\n<img src='banner.png' alt='Banner' width='1279' height='351'>\n")
log_file.close()


def print_help():
    """
    Prints help regarding the utilisation of the program
    """
    print("DESCRIPTION : EfT WikiCrawler - Quickly find a specific expression on all the pages it is present\n")
    print("HOW TO USE : python3 crawler.py \"[expression]\" OR python3 crawler.py -h/--help")

def check_query(page_content, query):
    """
    Checks if the query is contained in the content of the page
    :param page_content:
    :param query: search query passed as arg
    :return: boolean whether the query is in the page or not
    """
    if re.search(query, page_content):
        return True
    return False

def fetch_all_links(query):
    """
    Fetches all links and append them to the hashmap, before treating them
    :param query: search query passed as arg
    """
    hashmap = {}
    first_url = "Weapon_mods"
    get_links(first_url, hashmap, query)

    while hashmap:
        next_url = next(iter(hashmap))
        get_links(hashmap.pop(next_url).replace(" ", "_"), hashmap, query)

def get_links(url, hashmap, query):
    """
    Stores all the links present on an edit page in a hashmap, passed as parameter
    :param url: url of the link to visit
    :param hashmap: hashmap containing the links and their hashes
    :param query: search query passed as arg
    """
    page_content = eft_get_content(url, query)
    link_start = 0
    index = 0

    while link_start != -1 and index < len(page_content):
        link_start = page_content.find("[[", index)
        link_end = page_content.find("]]", link_start)

        if link_start != -1 and link_end != -1:
            link = page_content[link_start + 2:link_end]
            index_hyper = link.find("|")
            if index_hyper != -1:  # If we find an hyper link in the link
                link = link[0:index_hyper]
            new_hash = md5(link.encode('utf-8')).hexdigest()
            if link[0:2] != "de" and link[0:2] != "fr" and link[0:2] != "ru":
                if (".png" or ".jpg" or ".gif" or ".PNG") not in link:
                    if new_hash not in hashmap:
                        hashmap[new_hash] = link

        index = link_end

def eft_get_content(url, query):
    """
    Returns the content of the source edit area of a page
    :param url: url of the page to fetch
    :param query: search query passed as arg
    :return: the content of the page
    """
    url = 'https://escapefromtarkov.gamepedia.com/index.php?title=' + url + '&action=edit'
    try:
        page = req.urlopen(url)

        soup = bs(page.read(), features="html.parser")
        text_area = soup.textarea
        if text_area == None:
            print(bColors.GRN + "Search finished ! The results have been stored in result_log.html" + bColors.RST)
            exit(0)
        page_content = text_area.get_text()

        log_file = open("result_log.html", "a")

        if check_query(page_content, query):
            print(bColors.GRN + "Pattern found : " + bColors.RST + url)
            log_file.write("<p><a href ='" + url + "'>" + url + "</a></p>\n")

        log_file.close()
        return page_content  # Handle when no textarea ?

    except UnicodeEncodeError:
        print(bColors.RED + " /!\ Cyrillic : " + bColors.RST + url)
        return ""

def main():
    if len(sys.argv) > 1:
        query = sys.argv[1]
        if query == "-h" or query == "--help":
            print_help()
            return 0
    else:
        print_help()
        return 0

    fetch_all_links(query)

    log_file = open("result_log.html", "a")
    log_file.write("\n</body>\n</html>\n")
    log_file.close()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        log_file = open("result_log.html", "a")
        log_file.write("\n</body>\n</html>\n")
        log_file.close()
        print("\nYou stopped the program. The results so far have been saved in result_log.html")
