# WikiCrawler

## Objectives :

- Quick and precise search of the whole wiki
- Visual representation of links
- Search for potential Edits :

If you want to search for a specific pattern, like all weights without a space between the number and the unit, try

```
python3 crawler.py "[0-9]kg"
```

This will search for all pages where the pattern is present. Then, the results are written in `result_log.html` that you can open with your browser.
The search is quite slow (for now) : Wait for 10 minutes for the full result.
(I'm working on making it faster !)

##Installation :

###First time :

- Install WSL on Linux. Choose your distribution as you want, Ubuntu is fine for the tool. I suggest using Ubuntu, it is much easier to setup for the tool.
- Start your newly downloaded WSL distribution by launching `Ubuntu` or `ArchLinux` or whatever through the Windows start menu.
- Make sure `git` is installed by typing `git` :
    - If you get the message `bash: command 'git' not found` then you need to install it by using the following command :
        - On Ubuntu, type `sudo apt-get install git`
        - On ArchLinux, type `sudo pacman -S git`

- Once that is done, download the tool by typing `git clone https://gitlab.com/CohenArthur/wikicrawler`
- Go to that directory by typing `cd wikicrawler`
- Make sure Python 3 is installed by typing `python3` into your terminal :
    - If you get the message `bash: command 'python3' not found` then you need to install it by using the following command :
        - On Ubuntu, type `sudo apt-get install python3`
        - On ArchLinux, type `sudo pacman -S python3`

- Install the pip package manager :
    - On Ubuntu, pip is not required.
    - On ArchLinux, type `sudo pacman -S python-pip`

- Install the required library :
    - On Ubuntu, type `sudo apt-get install bs4`
    - On ArchLinux, type `sudo pip install bs4`

You're now ready to use the tool !

##How to use :

```
python3 crawler.py <your-query/regex>
```

If you need help for a regex, don't forget to search on google or to ask in #coding-corner

You can quit the program midway by pressing `CTRL + C` : This will save the links found so far.
The results are shown in `result_log.html`

To access them, and if you're lucky, type `explorer.exe .` in your terminal and double click the file `result_log.html` : This will open it in your default browser.
Then, click on the link and edit what you need !

If not, you can access your Ubuntu files by pressing `WIN + R` and typing in `%localappdata%\Packages\CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc\LocalState\rootfs\home\<your-ubuntu-username>`
For ArchLinux, type in `%localappdata%\Packages\52569scottxu.ArchLinux_xhwh673y3hxf8\LocalState\rootfs\home\<your-arch-username>`
