CC=gcc
CFLAGS=-Wall -Wextra -Werror -g -lcurl

SRC_DIR= ./src/
LIB_DIR= ./lib/
ALL_SRC=$(wildcard $(SRC_DIR)*.c) $(wildcard $(LIB_DIR)*.c)

SRC=crawler.c ${ALL_SRC}
OBJ=${ALL_SRC:*.c=*.o}

all : crawler

crawler : ${OBJ}

clean : 
	${RM} crawler *.o *.d 
