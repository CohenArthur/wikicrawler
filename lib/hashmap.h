# pragma once

#include <stdio.h> //FIXME

#include <stddef.h>
#include <stdlib.h>
#include <err.h>
#include <string.h>

# define HASH struct hashmap

# define HM_DEFAULT_CAPACITY 64

# define HM_REDUCE_AT        25
# define HM_AUGMENT_AT       75

# define HM_ITER_DESTRUCTIVE 1

# define HM_MAP_AUTO_RESIZE  1
# define HM_MAP_FREE_VALUE   2
# define HM_MAP_FREE_KEY     4

# define HM_CODE_ERROR       0
# define HM_CODE_SUCCESS     0
# define HM_CODE_INSERTED    1
# define HM_CODE_REPLACED    2

typedef long (*hashfunction)(void *ptr);

typedef int (*comparefunction)(void *ptr1, void *ptr2);

typedef void (*freefunction)(void *ptr);

struct hm_node
{
    struct hm_node  *next;
    long             hash;
    void            *key;
    void            *value;
};

struct hashmap
{
    int              flags;

    size_t           size;
    size_t           capacity;

    hashfunction     hash;
    comparefunction  compare;

    freefunction     fvalue;
    freefunction     fkey;

    struct hm_node **array;
};

struct hm_iter
{
    int              flags;
    size_t           pos;
    struct hashmap  *map;
    struct hm_node  *next;
};

/**
 * Create a new hashmap with specified capacity, hash function and
 * compare function.
 * Return a pointer on the hashmap if successful, NULL otherwise.
 *
 * Flags:
 *  HM_MAP_AUTO_RESIZE  : resize hashmap when there is too much or
 *                          too few elements. If set, given capacity
 *                          is ignored to use HM_DEFAULT_CAPACITY
 *  HM_MAP_FREE_VALUE   : free the value pointer when the value is removed
 *                          from the hashmap, except when the value is removed
 *                          by hm_replace
 *  HM_MAP_FREE_KEY     : free the key pointer when the key is removed from
 *                          the hashmap
 */
struct hashmap *hm_create(size_t capacity, hashfunction hash, comparefunction compare, int flags);

/**
 * Create an hashmap where keys are null-terminated strings.
 */
struct hashmap *hm_create_str(size_t capacity, int flags);

/**
 * Create an hashmap where keys are null-terminated strings, ignoring case.
 */
struct hashmap *hm_create_str_ignore_case(size_t capacity, int flags);

/**
 * Destroy the map. Do not use it after calling this function.
 */
void hm_destroy(struct hashmap *map);

/**
 * Destroy the node
 */
void hm_node_destroy(struct hashmap *map, struct hm_node *node);

/**
 * Remove all (key => value) of the map
 */
void hm_clear(struct hashmap *map);

/**
 * Get the node corresponding to the key. Return NULL if the key
 * isn't in the map.
 */
struct hm_node *hm_get_node(struct hashmap *map, void *key);

/**
 * Get the value corresponding to the key. Return NULL if the key
 * isn't in the map.
 */
void *hm_get(struct hashmap *map, void *key);

/**
 * Insert (key => value) into the map.
 *
 * Return:
 *  HM_CODE_ERROR   : if the insertion failed
 *  HM_CODE_INSERTED: if the insertion was successful and key wasn't in the map
 *  HM_CODE_REPLACED: if the old value associated to key has been replaced
 */
int hm_insert(struct hashmap *map, void *key, void *value);

/**
 * Insert (key => value) into the map. If key was already in the map,
 * meaning the function returns HM_CODE_REPLACED, old will receive the
 * old value.
 *
 * Otherwise, same behaviour than hm_insert.
 */
int hm_replace(struct hashmap *map, void *key, void *value, void **old);

/**
 * Remove element with key (key) from the map. Return a pointer on the value
 * if the key is in the map, NULL otherwise.
 */
void *hm_remove(struct hashmap *map, void *key);

/**
 * Change the capacity of the map. All elements will be reinserted, so the
 * operation is expensive.
 *
 * Return:
 *  HM_CODE_ERROR  : if the resize failed
 *  HM_CODE_SUCCESS: if the resize was successful
 */
int hm_resize(struct hashmap *map, size_t new_capacity);

/**
 * Iterators can be used like this:
 *
 *  struct hm_node *node;
 *  void *iter = hm_iter_init(map);
 *
 *  while (node = hm_iter_next(iter))
 *      do_stuff();
 *
 *  There is no guaranteed order.
*/

/**
 * Create an iterator on an hashmap. Return NULL if the allocation failed.
 *
 * Flags:
 *  HM_ITER_DESTRUCTIVE     Remove all elements from the map. Warning, the
 *                              nodes will not be freed.
 */
struct hm_iter *hm_iter_init(struct hashmap *map, int flags);

/**
 * Destroy an iterator.
 */
void hm_iter_destroy(struct hm_iter *iter);

/**
 * Get next element of iterator.
 * Return a pointer on an the next element if there is one, NULL otherwise.
 */
struct hm_node *hm_iter_next(struct hm_iter *iter);

/**
 * For internal use only
 */
void hm_size_change(struct hashmap *map, size_t size);
